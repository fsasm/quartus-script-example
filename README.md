# quartus-script-example
This shows how to create and build a design with Quartus with only scripts that are executed on the command line. 

To create and build the sample project go to folder `src` and execute `./build.sh`. The script first creates a Quartus project with `setup_proj.tcl` and then builds the project. The reports and output files, i.e. the SOF and POF files, are in the folder `output`.