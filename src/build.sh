#!/bin/sh
PROJECT=max1000blink
quartus_sh -t setup_proj.tcl

# synthesize the files
quartus_map $PROJECT

# place and route
quartus_fit $PROJECT

# write SOF
quartus_asm $PROJECT

