library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_blink is
	port(
		clk  : in  std_logic;
		in0  : in  std_logic;
		in1  : in  std_logic;
		out0 : out std_logic
	);
end top_blink;  

architecture behav of top_blink is
begin
	process(clk)
		variable in01 : std_logic_vector(1 downto 0);
	begin
		if rising_edge(clk) then
			in01 := in0 & in1;
			case in01 is
			when "00" => out0 <= '1';
			when "01" => out0 <= '0';
			when "10" => out0 <= '0';
			when "11" => out0 <= '1';
			end case;
		end if;
	end process;
end behav;
