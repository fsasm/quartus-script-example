# creates a project for the MAX-1000 Board

project_new max1000blink

# set device parameter
set_global_assignment -name FAMILY "MAX 10"
set_global_assignment -name DEVICE 10M16SAU169C8G

# set project parameter
set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output
set_global_assignment -name TOP_LEVEL_ENTITY top_blink

# add source files
set_global_assignment -name VHDL_FILE top.vhd -hdl_version VHDL_2008
set_global_assignment -name SDC_FILE SDC1.sdc

# set pin settings
set_global_assignment -name VCCA_USER_VOLTAGE 3.3V

set_location_assignment PIN_H6 -to clk
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to clk

set_location_assignment PIN_A8 -to out0
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to out0

set_location_assignment PIN_H8 -to in0
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to in0

set_location_assignment PIN_K10 -to in1
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to in1

# done
project_close
